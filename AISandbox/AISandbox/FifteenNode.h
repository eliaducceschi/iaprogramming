#pragma once

#include "Vector2.h"

#include <SFML/Graphics.hpp>
#include <vector>

class FifteenNode
{

private:

	const static unsigned int FONT_SIZE;
	const static sf::Color FONT_COLOR;
	const static sf::Color LINE_COLOR;

public:

	const static float CELL_SIZE;
	const static float LINE_THICKNESS;

	const static int NULL_VALUE = 0;
	const static int ROWS = 4;
	const static int COLUMNS = 4;

private:

	const FifteenNode* m_Parent = nullptr;
	int m_WeightSum = 0;

	int m_Grid[ROWS][COLUMNS];
	Vector2 m_NullValuePosition{ 0,0 };

public:

	FifteenNode();
	FifteenNode(const int i_Grid[ROWS][COLUMNS]);
	FifteenNode(const FifteenNode &i_Other);

	void Initialize(const int i_Grid[ROWS][COLUMNS]);

	bool operator==(const FifteenNode &i_Other) const;
	bool operator!=(const FifteenNode &i_Other) const;
	bool operator<(const FifteenNode &i_Other) const;

	std::vector<FifteenNode> GetNeighbors() const;
	int GetGPlusH(const FifteenNode &i_EndNode) const;

	void Draw(sf::RenderWindow& i_RenderWindow, float i_XOffset, float i_YOffset) const;

private:

	int CalculateHeuristic(const FifteenNode &i_EndNode) const;
	int CalculateDistance(const Vector2 i_GridPos, const FifteenNode & i_Node) const;
	FifteenNode GetNeighbor(Vector2 i_ToSwap) const;

	void DrawGrid(sf::RenderWindow& i_RenderWindow, float i_XOffset, float i_YOffset) const;

};

