#include "Sneezy.h"


const std::string Sneezy::NAME = "Sneezy";

//public

Sneezy::Sneezy() : Sneezy(0, MAX_ENERGY)
{
}

Sneezy::Sneezy(const int i_InitialLoad, const int i_InitialEnergy) : Miner(NAME, MAX_LOAD, MAX_ENERGY, i_InitialLoad, i_InitialEnergy)
{
}

void Sneezy::Extract()
{
	Miner::Extract(1, 3);
}

void Sneezy::Deposit()
{
	Miner::Deposit(3);
}

void Sneezy::Recover()
{
	Miner::Recover(5);
}
