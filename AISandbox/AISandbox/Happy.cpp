#include "Happy.h"


const std::string Happy::NAME = "Happy";

//public

Happy::Happy() : Happy(0, MAX_ENERGY)
{
}

Happy::Happy(const int i_InitialLoad, const int i_InitialEnergy) : Miner(NAME, MAX_LOAD, MAX_ENERGY, i_InitialLoad, i_InitialEnergy)
{
}

void Happy::Extract()
{
	Miner::Extract(2, 3);
}

void Happy::Deposit()
{
	Miner::Deposit(4);
}

void Happy::Recover()
{
	Miner::Recover(1);
}
