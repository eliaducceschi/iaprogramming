#pragma once

#include "Miner.h"

class Dopey : public Miner
{

private:

	static const int MAX_LOAD = 10;
	static const int MAX_ENERGY = 50;

	static const std::string NAME;

public:

	Dopey();
	Dopey(const int i_InitialLoad, const int i_InitialEnergy);

	virtual void Extract() override;
	virtual void Deposit() override;
	virtual void Recover() override;

};