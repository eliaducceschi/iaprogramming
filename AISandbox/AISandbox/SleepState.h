#pragma once

#include "Miner.h"
#include "State.h"

class SleepState : public State<Miner>
{

private:

	static SleepState *m_Instance;

public:

	static SleepState* GetInstance();

	virtual void Enter(Miner *i_Agent) override;
	virtual State<Miner>* Tick(Miner *i_Agent) override;
	virtual void Exit(Miner *i_Agent) override;

	virtual std::string GetDescription() const override;

	virtual ~SleepState() override;

protected:

	SleepState() = default;

};

