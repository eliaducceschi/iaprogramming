#pragma once

#include "IScene.h"
#include "FifteenStar.h"

#include <random>


class FifteenPuzzleScene : public IScene
{

private:

	const static sf::Time STEP_TIME;
	static std::default_random_engine m_RandomEngine;

private:

	sf::Clock m_Clock;
	sf::Time m_ElapsedTime;

	FifteenStar m_FifteenStarStep;
	FifteenStar m_FifteenStarAll;

	int m_StartGrid[FifteenNode::ROWS][FifteenNode::COLUMNS];
	int m_EndGrid[FifteenNode::ROWS][FifteenNode::COLUMNS];

public:

	FifteenPuzzleScene();


	virtual void OnEnter();
	virtual void OnIdle();
    virtual void OnDraw(sf::RenderWindow &i_RenderWindow);

	virtual ~FifteenPuzzleScene();

private:

	void InitializeGrids(int i_RandomMovements);
	void RandomizeStart(int m_Moves);

	void DrawText(sf::RenderWindow &i_RenderWindow);

};