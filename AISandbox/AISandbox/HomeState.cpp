#include "HomeState.h"
#include "SleepState.h"
#include "MineState.h"
#include "DepositState.h"

HomeState * HomeState::m_Instance = nullptr;

//public

HomeState* HomeState::GetInstance()
{
	if (m_Instance == nullptr)
	{
		m_Instance = new HomeState();
	}
	return m_Instance;
}

void HomeState::Enter(Miner *i_Agent)
{
}

State<Miner>* HomeState::Tick(Miner *i_Agent)
{
	if (i_Agent->IsTired())
	{
		return SleepState::GetInstance();
	}
	if (!i_Agent->HasNoLoad())
	{
		return DepositState::GetInstance();
	}
	return MineState::GetInstance();
}

void HomeState::Exit(Miner *i_Agent)
{
}

std::string HomeState::GetDescription() const
{
	return "Home";
}

HomeState::~HomeState()
{
}
