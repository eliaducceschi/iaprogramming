#pragma once

#include "Vector2.h"
#include "NodeState.h"

#include <list>
#include <vector>

class Node
{

public:

	typedef std::pair<Node*, int> DistanceNode;

public:

	Node* m_Parent = nullptr;

	NodeState m_NodeState = Unknown;

	Vector2 m_Position{0, 0};
	std::vector<DistanceNode> m_Neighbors = std::vector<DistanceNode>();

	int m_WeightSum = 0;
	int m_Heuristic = 0;

};
