#include "AStarScene.h"
#include "AStar.h"
#include <chrono>
#include <thread>

AStarScene::AStarScene()
{
}

AStarScene::~AStarScene()
{
    
}

void AStarScene::OnEnter()
{
	alg.Run(10, 10, { 2, 9 }, { 6, 5 }, false);
}

void AStarScene::OnIdle()
{
	
}

void AStarScene::OnDraw(sf::RenderWindow& window)
{
	window.clear(sf::Color::White);
	if (alg.end)
	{
		alg.DrawPath(window);
	}
	else
	{
		alg.Step();
		alg.DrawStep(window);
		std::this_thread::sleep_for(std::chrono::milliseconds(250));
	}
	
}
