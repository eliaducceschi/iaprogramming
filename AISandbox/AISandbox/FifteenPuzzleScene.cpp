#include "FifteenPuzzleScene.h"

#include "FontEnum.h"
#include "FontManager.h"

const sf::Time FifteenPuzzleScene::STEP_TIME = sf::seconds(0.050f);
std::default_random_engine FifteenPuzzleScene::m_RandomEngine = std::default_random_engine((unsigned)std::time(nullptr));

//public

FifteenPuzzleScene::FifteenPuzzleScene() : m_FifteenStarStep(), m_FifteenStarAll()
{
}

void FifteenPuzzleScene::OnEnter()
{
	InitializeGrids(10);
	RandomizeStart(10);
	m_FifteenStarStep.Initialize(m_StartGrid, m_EndGrid);
	m_FifteenStarAll.Initialize(m_StartGrid, m_EndGrid);
	m_FifteenStarAll.ExecuteAll();
	m_Clock.restart();
}

void FifteenPuzzleScene::OnIdle()
{
	if (!m_FifteenStarStep.Done())
	{
		m_ElapsedTime += m_Clock.restart();
		if (m_ElapsedTime > STEP_TIME)
		{
			m_ElapsedTime = sf::Time::Zero;
			m_FifteenStarStep.ExecuteNextStep();
		}
	}
}

void FifteenPuzzleScene::OnDraw(sf::RenderWindow &i_RenderWindow)
{
	i_RenderWindow.clear(sf::Color::White);
	m_FifteenStarStep.Draw(i_RenderWindow, 430.0f);
	m_FifteenStarAll.Draw(i_RenderWindow, 125.0f);

	DrawText(i_RenderWindow);
}

FifteenPuzzleScene::~FifteenPuzzleScene()
{

}

//private

void FifteenPuzzleScene::InitializeGrids(int i_RandomMovements)
{
	for (int i = 0; i < FifteenNode::ROWS; ++i)
	{
		for (int j = 0; j < FifteenNode::COLUMNS; ++j)
		{
			m_EndGrid[i][j] = 1 + i*FifteenNode::COLUMNS + j;
		}
	}
	m_EndGrid[FifteenNode::ROWS - 1][FifteenNode::COLUMNS - 1] = FifteenNode::NULL_VALUE;


	std::uniform_int_distribution<unsigned> randomRow(0, FifteenNode::ROWS - 1);
	std::uniform_int_distribution<unsigned> randomColumn(0, FifteenNode::COLUMNS - 1);
	while (i_RandomMovements > 0)
	{
		std::swap(m_EndGrid[randomRow(m_RandomEngine)][randomColumn(m_RandomEngine)],
			m_EndGrid[randomRow(m_RandomEngine)][randomColumn(m_RandomEngine)]);
		--i_RandomMovements;
	}

	for (int i = 0; i < FifteenNode::ROWS; ++i)
	{
		for (int j = 0; j < FifteenNode::COLUMNS; ++j)
		{
			m_StartGrid[i][j] = m_EndGrid[i][j];
		}
	}
}

void FifteenPuzzleScene::RandomizeStart(int m_Moves)
{
	Vector2 nullPos(0, 0);
	for (int i = 0; i < FifteenNode::ROWS; ++i)
	{
		for (int j = 0; j < FifteenNode::COLUMNS; ++j)
		{
			if (m_StartGrid[i][j] == FifteenNode::NULL_VALUE)
			{
				nullPos.x = i;
				nullPos.y = j;
			}
		}
	}

	std::uniform_int_distribution<unsigned> randomDirection(0, 3);
	while (m_Moves > 0)
	{
		bool directionWasOk = false;
		Vector2 switchPos(0, 0);

		do
		{
			int direction = randomDirection(m_RandomEngine);
			if (direction == 0)
			{
				if (nullPos.y < FifteenNode::COLUMNS - 2)
				{
					switchPos = Vector2(nullPos.x, nullPos.y + 1);
					directionWasOk = true;
				}
			}
			else if (direction == 1)
			{
				if (nullPos.y > 0)
				{
					switchPos = Vector2(nullPos.x, nullPos.y - 1);
					directionWasOk = true;
				}
			}
			else if (direction == 2)
			{
				if (nullPos.x < FifteenNode::ROWS - 2)
				{
					switchPos = Vector2(nullPos.x + 1, nullPos.y);
					directionWasOk = true;
				}
			}
			else
			{
				if (nullPos.x > 0)
				{
					switchPos = Vector2(nullPos.x - 1, nullPos.y);
					directionWasOk = true;
				}
			}

		}
		while (!directionWasOk);

		std::swap(m_StartGrid[nullPos.x][nullPos.y], m_StartGrid[switchPos.x][switchPos.y]);
		nullPos.x = switchPos.x;
		nullPos.y = switchPos.y;
		--m_Moves;
	}

}

void FifteenPuzzleScene::DrawText(sf::RenderWindow & i_RenderWindow)
{
	const sf::Font* font = FontManager::Istance()->GetFont(FontEnum::Font_Sansation);

	sf::Text all = sf::Text("All", (*font), 40U);
	all.setOrigin(all.getLocalBounds().width / 2.0f, all.getLocalBounds().height / 2.0f);
	all.setFillColor(sf::Color::Black);
	all.setPosition({ 492, 30.0f });
	i_RenderWindow.draw(all);

	sf::Text allNumber = sf::Text(std::to_string(m_FifteenStarAll.GetCurrentStep()), (*font), 30U);
	allNumber.setOrigin(allNumber.getLocalBounds().width / 2.0f, allNumber.getLocalBounds().height / 2.0f);
	allNumber.setFillColor(sf::Color::Black);
	allNumber.setPosition({ 492, 80.0f });
	i_RenderWindow.draw(allNumber);

	sf::Text step = sf::Text("Step by step", (*font), 40U);
	step.setOrigin(step.getLocalBounds().width / 2.0f, step.getLocalBounds().height / 2.0f);
	step.setFillColor(sf::Color::Black);
	step.setPosition({ 492, 340.0f });
	i_RenderWindow.draw(step);

	sf::Text stepNumber = sf::Text(std::to_string(m_FifteenStarStep.GetCurrentStep()), (*font), 30U);
	stepNumber.setOrigin(stepNumber.getLocalBounds().width / 2.0f, stepNumber.getLocalBounds().height / 2.0f);
	stepNumber.setFillColor(sf::Color::Black);
	stepNumber.setPosition({ 492, 385.0f });
	i_RenderWindow.draw(stepNumber);
}
