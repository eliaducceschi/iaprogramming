#pragma once

#include "Miner.h"

class Bashful : public Miner
{

private:

	static const int MAX_LOAD = 20;
	static const int MAX_ENERGY = 20;

	static const std::string NAME;

public:

	Bashful();
	Bashful(const int i_InitialLoad, const int i_InitialEnergy);

	virtual void Extract() override;
	virtual void Deposit() override;
	virtual void Recover() override;

};