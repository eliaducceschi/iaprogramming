#include "Utils.h"

#include <string>
#include <assert.h>

std::string ToCurrentSlashMax(int i_Current, int i_Max)
{
	assert(("Current must be a non negative number", i_Current >= 0));
	assert(("Current must be a less or equal to Max", i_Current <= i_Max));

	std::string currentString = std::to_string(i_Current);
	std::string maxString = std::to_string(i_Max);

	int digitNumberDifference = maxString.size() - currentString.size();
	for (int i = 0; i < digitNumberDifference; ++i)
	{
		currentString = "0" + currentString;
	}

	return currentString + "/" + maxString;
}
