#include "Miner.h"
#include "HomeState.h"

#include "Utils.h"
#include "FontEnum.h"
#include "FontManager.h"

#include <string>
#include <algorithm>
#include <assert.h>

Miner::Miner(std::string i_Name, int i_MaxLoad, int i_MaxEnergy) : Miner(i_Name, i_MaxLoad, i_MaxEnergy, 0, i_MaxEnergy)
{
}

Miner::Miner(std::string i_Name, int i_MaxLoad, int i_MaxEnergy, int i_InitialLoad, int i_InitialEnergy)
	: m_Name(i_Name), m_MaxLoad(i_MaxLoad), m_MaxEnergy(i_MaxEnergy), m_Load(i_InitialLoad), 
	  m_Energy(i_InitialEnergy), m_Deposit(0), m_Brain(this, HomeState::GetInstance())
{
	assert(("Max load must be a non negative number", m_MaxLoad >= 0));
	assert(("Max energy must be a non negative number", m_MaxEnergy >= 0));
	assert(("Load must be a number between 0 and " + std::to_string(m_MaxLoad), m_Load >= 0 && m_Load <= m_MaxLoad));
	assert(("Energy must be a number between 0 and " + std::to_string(m_MaxEnergy), m_Energy >= 0 && m_Energy <= m_MaxEnergy));
}

void Miner::Tick()
{
	m_Brain.Tick(this);
}

void Miner::Extract(int i_ToExtract, int i_EnergyUsed)
{
	assert(("Extract must be a non negative number", i_ToExtract >= 0));
	assert(("Energy must be a non negative number", i_EnergyUsed >= 0));

	int newLoad = m_Load + i_ToExtract;
	int newEnergy = m_Energy - i_EnergyUsed;
	m_Load = std::min(newLoad, m_MaxLoad);
	m_Energy = std::max(newEnergy, 0);
}

void Miner::Deposit(int i_Quantity)
{
	assert(("Quantity must be a non negative number", i_Quantity >= 0));

	m_Deposit += std::min(m_Load, i_Quantity);
	int newLoad = m_Load - i_Quantity;
	m_Load = std::max(newLoad, 0);
}

void Miner::Recover(int i_Quantity)
{
	assert(("Quantity must be a non negative number", i_Quantity >= 0));

	int newEnergy = m_Energy + i_Quantity;
	m_Energy = std::min(newEnergy, m_MaxEnergy);
}

void Miner::Draw(sf::RenderWindow &i_RenderWindow, float i_StartX, float i_StartY)
{
	const sf::Font* font = FontManager::Istance()->GetFont(FontEnum::Font_Verdana);

	sf::Text name = sf::Text(GetName(), (*font), 32U);
	name.setFillColor(sf::Color::Black);
	name.setPosition({ i_StartX, i_StartY });
	i_RenderWindow.draw(name);

	float offsetX = 7.0f;
	float offsetY = 7.0f;
	float lineHeight = 30.0f;

	sf::Text state = sf::Text("- State: " + GetState(), (*font), 24U);
	state.setFillColor(sf::Color::Black);
	state.setPosition({ i_StartX + offsetX, i_StartY + offsetY + lineHeight });
	i_RenderWindow.draw(state);

	sf::Text energy = sf::Text("- Energy: " + ToCurrentSlashMax(m_Energy, m_MaxEnergy), (*font), 24U);
	energy.setFillColor(sf::Color::Black);
	energy.setPosition({ i_StartX + offsetX, i_StartY + offsetY + lineHeight * 2.0f });
	i_RenderWindow.draw(energy);

	sf::Text load = sf::Text("- Load: " + ToCurrentSlashMax(m_Load, m_MaxLoad), (*font), 24U);
	load.setFillColor(sf::Color::Black);
	load.setPosition({ i_StartX + offsetX, i_StartY + offsetY + lineHeight * 3.0f });
	i_RenderWindow.draw(load);

	sf::Text deposit = sf::Text("- Deposit: " + std::to_string(GetDeposit()), (*font), 24U);
	deposit.setFillColor(sf::Color::Black);
	deposit.setPosition({ i_StartX + offsetX, i_StartY + offsetY + lineHeight * 4.0f });
	i_RenderWindow.draw(deposit);
}

void Miner::Reset()
{
	Reset(0, m_MaxEnergy);
}

void Miner::Reset(int i_Load, int i_Energy)
{
	assert(("Load must be a number between 0 and " + std::to_string(m_MaxLoad), m_Load >= 0 && m_Load <= m_MaxLoad));
	assert(("Energy must be a number between 0 and " + std::to_string(m_MaxEnergy), m_Energy >= 0 && m_Energy <= m_MaxEnergy));

	m_Deposit = 0;
	m_Load = i_Load;
	m_Energy = i_Energy;
}

bool Miner::IsFullLoad() const
{
	return m_Load == m_MaxLoad;
}

bool Miner::IsFullEnergy() const
{
	return m_Energy == m_MaxEnergy;
}

bool Miner::IsTired() const
{
	return m_Energy == 0;
}

bool Miner::HasNoLoad() const
{
	return m_Load == 0;
}

std::string Miner::GetName() const
{
	return m_Name;
}

std::string Miner::GetState() const
{
	return m_Brain.GetCurrentState()->GetDescription();
}

int Miner::GetMaxLoad() const
{
	return m_MaxLoad;
}

int Miner::GetMaxEnergy() const
{
	return m_MaxEnergy;
}

int Miner::GetLoad() const
{
	return m_Load;
}

int Miner::GetEnergy() const
{
	return m_Energy;
}

int Miner::GetDeposit() const
{
	return m_Deposit;
}

