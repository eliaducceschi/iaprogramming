#include "MinerScene.h"
#include "Doc.h"
#include "Grumpy.h"
#include "Happy.h"
#include "Sleepy.h"
#include "Bashful.h"
#include "Sneezy.h"
#include "Dopey.h"

#include "FontEnum.h"
#include "FontManager.h"

#include <string>
#include <iostream>

const sf::Time MinerScene::STEP_TIME = sf::seconds(0.5f);

//public

MinerScene::MinerScene() : m_Miners()
{
	m_SevenDwarfs.openFromFile("AISandbox/SevenDwarfs.wav");
	m_SevenDwarfs.setLoop(true);

	m_Miners.push_back(std::make_unique<Doc>());
	m_Miners.push_back(std::make_unique<Grumpy>());
	m_Miners.push_back(std::make_unique<Happy>());
	m_Miners.push_back(std::make_unique<Sleepy>());
	m_Miners.push_back(std::make_unique<Bashful>());
	m_Miners.push_back(std::make_unique<Sneezy>());
	m_Miners.push_back(std::make_unique<Dopey>());
}

MinerScene::~MinerScene()
{    
}

void MinerScene::OnEnter()
{
	for (auto miner = m_Miners.begin(); miner != m_Miners.end(); ++miner)
	{
		(*miner)->Reset();
	}

	m_Clock.restart();

	m_SevenDwarfs.play();
}

void MinerScene::OnExit()
{
	m_SevenDwarfs.stop();
}

void MinerScene::OnIdle()
{
	if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Space))
	{
		m_ElapsedTime += m_Clock.restart();
		if (m_ElapsedTime > STEP_TIME)
		{
			m_ElapsedTime = sf::Time::Zero;
			for (auto miner = m_Miners.begin(); miner != m_Miners.end(); ++miner)
			{
				(*miner)->Tick();
			}
		}
	}
	else
	{
		m_Clock.restart();
	}
}

void MinerScene::OnDraw(sf::RenderWindow &i_RenderWindow)
{
	i_RenderWindow.clear(sf::Color::White);

	const sf::Font* font = FontManager::Istance()->GetFont(FontEnum::Font_Verdana);

	sf::Text space = sf::Text("Hold space to pause", (*font), 38U);
	space.setFillColor(sf::Color::Black);
	space.setPosition({ std::floor(512.0f - space.getLocalBounds().width / 2.0f), 30.0f });
	i_RenderWindow.draw(space); 

	int totalDeposit = 0;

	for (size_t minerIndex = 0; minerIndex < m_Miners.size(); ++minerIndex)
	{
		float x = 40.0f + 120.0f * (((minerIndex % 7) > 3) ? 1 : 0) + (((minerIndex % 7) > 3) ? (minerIndex % 7) - 4 : minerIndex % 7) * 250.0f;
		float y = 115.0f + (minerIndex / 7) * (230.0f * 2.0f) + ((minerIndex % 7) / 4) * 230.0f;
		m_Miners.at(minerIndex)->Draw(i_RenderWindow, x, y);

		totalDeposit += m_Miners.at(minerIndex)->GetDeposit();
	}

	float totalDepositY = 115.0f + ((m_Miners.size() - 1) / 7) * (230.0f * 2.0f) + (((m_Miners.size() - 1) % 7) / 4) * 230.0f;

	sf::Text totalDepositText = sf::Text("Total deposit", (*font), 38U);
	totalDepositText.setFillColor(sf::Color::Black);
	totalDepositText.setPosition({ std::floor(514.0f - totalDepositText.getLocalBounds().width / 2.0f), totalDepositY + 200.0f });
	i_RenderWindow.draw(totalDepositText);

	sf::Text depositValueText = sf::Text(std::to_string(totalDeposit), (*font), 38U);
	depositValueText.setFillColor(sf::Color::Black);
	float depositTextX = 501.0f;
	if (totalDeposit > 0)
	{
		depositTextX = std::floor(512.0f - (std::floor(std::log10f(static_cast<float>(totalDeposit))) + 1)*11.0f);
	}
	depositValueText.setPosition({ depositTextX, totalDepositY + 252.0f });
	i_RenderWindow.draw(depositValueText);
}





