#include "MineState.h"
#include "HomeState.h"

MineState * MineState::m_Instance = nullptr;

//public

MineState* MineState::GetInstance()
{
	if (m_Instance == nullptr)
	{
		m_Instance = new MineState();
	}
	return m_Instance;
}

void MineState::Enter(Miner *i_Agent)
{
}

State<Miner>* MineState::Tick(Miner *i_Agent)
{
	if (i_Agent->IsFullLoad() || i_Agent->IsTired())
	{
		return HomeState::GetInstance();
	}
	i_Agent->Extract();
	return nullptr;
}

void MineState::Exit(Miner *i_Agent)
{
}

std::string MineState::GetDescription() const
{
	return "Mine";
}

MineState::~MineState()
{
}
