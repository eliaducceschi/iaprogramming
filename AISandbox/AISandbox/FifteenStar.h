#pragma once

#include "FifteenNode.h"

#include <SFML/Graphics.hpp>
#include <list>
#include <vector>
#include <memory>

class FifteenStar
{

private:

	FifteenNode m_StartNode;
	FifteenNode m_EndNode;

	std::list<FifteenNode> m_OpenList;
	std::list<FifteenNode> m_ClosedList;

	int m_CurrentStep;

	bool m_End;
	bool m_Success;

public:

	FifteenStar();
	FifteenStar(const int i_StartGrid[FifteenNode::ROWS][FifteenNode::COLUMNS],
		const int i_EndGrid[FifteenNode::ROWS][FifteenNode::COLUMNS]);

	void Initialize(const int i_StartGrid[FifteenNode::ROWS][FifteenNode::COLUMNS],
		const int i_EndGrid[FifteenNode::ROWS][FifteenNode::COLUMNS]);

	void ExecuteAll();
	void ExecuteNextStep();

	void Draw(sf::RenderWindow& i_RenderWindow, float i_TopOffset) const;

	int GetCurrentStep() const;

	bool Done() const;
	bool Success() const;

private:

	void VisitNode(const FifteenNode &i_ToVisit);

	void InsertIntoOpenList(const FifteenNode &i_ToInsert);

	bool ShouldInsertIntoOpen(const FifteenNode &i_ToCheck) const;
	void RemoveFromClosed(const FifteenNode &i_ToCheck);

};
