#ifndef FONTENUM_H
#define FONTENUM_H

enum FontEnum
{
	Font_Invalid = -1,
	Font_Sansation = 0,
	Font_Verdana = 1,
	
	Font_Count
};

#endif