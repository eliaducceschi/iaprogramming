#pragma once

#include "Miner.h"
#include "State.h"

class MineState : public State<Miner>
{

private:

	static MineState *m_Instance;

public:

	static MineState* GetInstance();

	virtual void Enter(Miner *i_Agent) override;
	virtual State<Miner>* Tick(Miner *i_Agent) override;
	virtual void Exit(Miner *i_Agent) override;

	virtual std::string GetDescription() const override;

	virtual ~MineState() override;

protected:

	MineState() = default;

};
