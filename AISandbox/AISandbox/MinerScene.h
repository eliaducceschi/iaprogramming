#pragma once

#include "IScene.h"
#include "Miner.h"

#include <SFML/Audio.hpp>

#include <memory>

class MinerScene : public IScene
{

	typedef std::vector<std::unique_ptr<Miner>> Miners;

private:

	const static sf::Time STEP_TIME;

private:

	sf::Music m_SevenDwarfs;

	sf::Clock m_Clock;
	sf::Time m_ElapsedTime;

	Miners m_Miners;

public:

	MinerScene();
	virtual ~MinerScene();

	virtual void OnEnter();
	virtual void OnExit();
	virtual void OnIdle();
	virtual void OnDraw(sf::RenderWindow &i_RenderWindow);
};

