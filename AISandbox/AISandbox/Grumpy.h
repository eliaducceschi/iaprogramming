#pragma once

#include "Miner.h"

class Grumpy : public Miner
{

private:

	static const int MAX_LOAD = 30;
	static const int MAX_ENERGY = 30;

	static const std::string NAME;

public:

	Grumpy();
	Grumpy(const int i_InitialLoad, const int i_InitialEnergy);

	virtual void Extract() override;
	virtual void Deposit() override;
	virtual void Recover() override;

};