#include "SleepState.h"
#include "HomeState.h"

SleepState * SleepState::m_Instance = nullptr;

//public

SleepState* SleepState::GetInstance()
{
	if (m_Instance == nullptr)
	{
		m_Instance = new SleepState();
	}
	return m_Instance;
}

void SleepState::Enter(Miner *i_Agent)
{
}

State<Miner>* SleepState::Tick(Miner *i_Agent)
{
	if (i_Agent->IsFullEnergy())
	{
		return HomeState::GetInstance();
	}
	i_Agent->Recover();
	return nullptr;
}

void SleepState::Exit(Miner *i_Agent)
{
}

std::string SleepState::GetDescription() const
{
	return "Sleep";
}

SleepState::~SleepState()
{
}
