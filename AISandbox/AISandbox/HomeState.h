#pragma once

#include "Miner.h"
#include "State.h"

class HomeState: public State<Miner>
{

private:

	static HomeState *m_Instance;

public:

	static HomeState* GetInstance();

	virtual void Enter(Miner *i_Agent) override;
	virtual State<Miner>* Tick(Miner *i_Agent) override;
	virtual void Exit(Miner *i_Agent) override;

	virtual std::string GetDescription() const override;

	virtual ~HomeState() override;

protected:

	HomeState() = default;

};

