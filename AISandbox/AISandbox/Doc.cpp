#include "Doc.h"


const std::string Doc::NAME = "Doc";

//public

Doc::Doc() : Doc(0, MAX_ENERGY)
{
}

Doc::Doc(const int i_InitialLoad, const int i_InitialEnergy) : Miner(NAME, MAX_LOAD, MAX_ENERGY, i_InitialLoad, i_InitialEnergy)
{
}

void Doc::Extract()
{
	Miner::Extract(4, 2);
}

void Doc::Deposit()
{
	Miner::Deposit(1);
}

void Doc::Recover()
{
	Miner::Recover(4);
}
