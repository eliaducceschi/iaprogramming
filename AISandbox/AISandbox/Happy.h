#pragma once

#include "Miner.h"

class Happy : public Miner
{

private:

	static const int MAX_LOAD = 25;
	static const int MAX_ENERGY = 5;

	static const std::string NAME;

public:

	Happy();
	Happy(const int i_InitialLoad, const int i_InitialEnergy);

	virtual void Extract() override;
	virtual void Deposit() override;
	virtual void Recover() override;

};