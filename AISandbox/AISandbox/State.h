#pragma once

#include <string>

template <class A> class State
{

public:

	virtual void Enter(A *i_Agent) = 0;
	virtual State<A>* Tick(A *i_Agent) = 0;
	virtual void Exit(A *i_Agent) = 0;

	virtual std::string GetDescription() const = 0;

	virtual ~State() = 0;

protected:

	State<A>() = default;

};

template <class A> State<A>::~State()
{

}
