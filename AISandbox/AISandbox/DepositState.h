#pragma once

#include "Miner.h"
#include "State.h"

class DepositState : public State<Miner>
{

private:

	static DepositState *m_Instance;

public:

	static DepositState* GetInstance();

	virtual void Enter(Miner *i_Agent) override;
	virtual State<Miner>* Tick(Miner *i_Agent) override;
	virtual void Exit(Miner *i_Agent) override;

	virtual std::string GetDescription() const override;

	virtual ~DepositState() override;

protected:

	DepositState() = default;

};

