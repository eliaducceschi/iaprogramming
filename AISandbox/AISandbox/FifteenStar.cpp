#include "FifteenStar.h"
#include "FontEnum.h"
#include "FontManager.h"


//public

FifteenStar::FifteenStar() : m_StartNode(), m_EndNode(), m_OpenList(), m_ClosedList(),
	m_CurrentStep(0), m_End(false), m_Success(false)
{
}

FifteenStar::FifteenStar(const int i_StartGrid[FifteenNode::ROWS][FifteenNode::COLUMNS], 
	const int i_EndGrid[FifteenNode::ROWS][FifteenNode::COLUMNS])
{
	Initialize(i_StartGrid, i_EndGrid);
}

void FifteenStar::Initialize(const int i_StartGrid[FifteenNode::ROWS][FifteenNode::COLUMNS], 
	const int i_EndGrid[FifteenNode::ROWS][FifteenNode::COLUMNS])
{
	m_StartNode.Initialize(i_StartGrid);
	m_EndNode.Initialize(i_EndGrid);

	m_OpenList.clear();
	m_OpenList.push_front(m_StartNode);
	m_ClosedList.clear();

	m_CurrentStep = 0,
	m_End = false;
	m_Success = false;
}

void FifteenStar::ExecuteAll()
{
	while (!Done())
	{
		ExecuteNextStep();
	}
}

void FifteenStar::ExecuteNextStep()
{
	if (m_OpenList.empty())
	{
		m_End = true;
		m_Success = false;
		return;
	}

	 const FifteenNode &toVisit = m_OpenList.front();

	 if (toVisit != m_EndNode)
	 {
		 VisitNode(toVisit);
	 }
	 else
	 {
		 m_End = true;
		 m_Success = true;
	 }

	 m_ClosedList.push_front(toVisit);
	 m_OpenList.pop_front();

	 m_CurrentStep++;
}

void FifteenStar::Draw(sf::RenderWindow & i_RenderWindow, float i_TopOffset) const
{
	if (!m_ClosedList.empty())
	{
		const FifteenNode &toDraw = m_ClosedList.front();

		const sf::Font* font = FontManager::Istance()->GetFont(FontEnum::Font_Sansation);
		sf::Text start = sf::Text("Start", (*font), 30U);
		start.setOrigin(start.getLocalBounds().width / 2.0f, start.getLocalBounds().height / 2.0f);
		start.setFillColor(sf::Color::Black);
		start.setPosition({ 230, i_TopOffset - 40.0f });
		i_RenderWindow.draw(start);

		sf::Text end = sf::Text("End", (*font), 30U);
		end.setOrigin(end.getLocalBounds().width / 2.0f, end.getLocalBounds().height / 2.0f);
		end.setFillColor(sf::Color::Black);
		end.setPosition({ 765, i_TopOffset - 40.0f });
		i_RenderWindow.draw(end);

		m_StartNode.Draw(i_RenderWindow, 150.0f, i_TopOffset);
		toDraw.Draw(i_RenderWindow, 250.0f + FifteenNode::COLUMNS * (FifteenNode::CELL_SIZE + FifteenNode::LINE_THICKNESS), i_TopOffset);
		m_EndNode.Draw(i_RenderWindow, 350.0f + 2.0f*(FifteenNode::COLUMNS * (FifteenNode::CELL_SIZE + FifteenNode::LINE_THICKNESS)), i_TopOffset);
	}

}

int FifteenStar::GetCurrentStep() const
{
	return m_CurrentStep;
}

bool FifteenStar::Done() const 
{
	return m_End;
}

bool FifteenStar::Success() const
{
	return m_End;
}

//private

void FifteenStar::VisitNode(const FifteenNode &i_ToVisit)
{
	std::vector<FifteenNode> neighbors = i_ToVisit.GetNeighbors();

	for (auto neighbor = neighbors.begin(); neighbor != neighbors.end(); ++neighbor)
	{
		if (ShouldInsertIntoOpen(*neighbor))
		{
			RemoveFromClosed(*neighbor);
			InsertIntoOpenList(*neighbor);
		}
	}
}

bool FifteenStar::ShouldInsertIntoOpen(const FifteenNode & i_ToCheck) const
{
	auto position = std::find(m_ClosedList.begin(), m_ClosedList.end(), i_ToCheck);
	if (position == m_ClosedList.end() || i_ToCheck < *position)
	{
		return true;
	}
	return false;
}

void FifteenStar::RemoveFromClosed(const FifteenNode & i_ToCheck)
{
	auto position = std::find(m_ClosedList.begin(), m_ClosedList.end(), i_ToCheck);
	if (position != m_ClosedList.end())
	{
		m_ClosedList.erase(position);
	}
}

void FifteenStar::InsertIntoOpenList(const FifteenNode & i_ToInsert)
{
	auto toCompare = m_OpenList.begin();
	while ((toCompare != m_OpenList.end()) &&
		(i_ToInsert.GetGPlusH(m_EndNode) > toCompare->GetGPlusH(m_EndNode)))
	{
		++toCompare;
	}
	m_OpenList.insert(toCompare, i_ToInsert);
}
