#pragma once

#include "State.h"

#include <assert.h>

template <class A> class FiniteStateMachine
{
private:

	State<A> *m_CurrentState;

public:

	FiniteStateMachine(A *i_Agent, State<A> *i_InitialState);

	void Tick(A *i_Agent);
	void ChangeState(A *i_Agent, State<A> *i_NewState);

	State<A> const* GetCurrentState() const;

};

template<class A>
inline FiniteStateMachine<A>::FiniteStateMachine(A *i_Agent, State<A> *i_InitialState) : m_CurrentState(i_InitialState)
{
	assert(("Initial state can't be null", i_InitialState != nullptr));

	m_CurrentState->Enter(i_Agent);
}

template<class A>
inline void FiniteStateMachine<A>::Tick(A *i_Agent)
{
	State<A> *newState = m_CurrentState->Tick(i_Agent);
	if (newState != nullptr)
	{
		ChangeState(i_Agent, newState);
	}
}

template<class A>
inline void FiniteStateMachine<A>::ChangeState(A *i_Agent, State<A> *i_NewState)
{
	assert(("New state can't be null", i_NewState != nullptr));

	m_CurrentState->Exit(i_Agent);
	i_NewState->Enter(i_Agent);
	m_CurrentState = i_NewState;
}

template<class A>
inline State<A> const* FiniteStateMachine<A>::GetCurrentState() const
{
	return m_CurrentState;
}
