#include "AStar.h"

#include <iostream>
#include <algorithm>
#include <random>
#include <time.h>

std::default_random_engine AStar::eng = std::default_random_engine((unsigned)std::time(nullptr));


void AStar::Run(int xMax, int yMax, Vector2 startNode, Vector2 endNode, bool random)
{
	Clean();

	this->xMax = xMax;
	this->yMax = yMax;
	this->random = random;
	this->startNode = startNode;
	this->endNode = endNode;

	CreateGraph();

	ComputeGraphHeuristics();

	openList.push_back(&graph.at(startNode.x).at(startNode.y));
}

void AStar::CreateGraph()
{
	for (int i = 0; i < xMax; ++i)
	{
		std::vector<Node> row = std::vector<Node>();
		for (int j = 0; j < yMax; ++j)
		{
			Node node;
			node.m_Position.x = i;
			node.m_Position.y = j;
			row.push_back(node);
		}
		graph.push_back(row);
	}
	CreateGraphAdjs();
}

void AStar::CreateGraphAdjs()
{
	for (int i = 0; i < xMax; ++i)
	{
		for (int j = 0; j < yMax; ++j)
		{
			CreateNodeAdj(i, j);
		}
	}
}

void AStar::CreateNodeAdj(const int row, const int col)
{
	std::uniform_int_distribution<unsigned> randomDistance(0,1);
	Node &node = graph.at(row).at(col);
	if (col > 0)
	{
		node.m_Neighbors.push_back(std::make_pair(&graph.at(row).at(col - 1), 
			(random) ? ((randomDistance(eng)) ? 1 : 2) : 1));
	}
	if (row > 0)
	{
		node.m_Neighbors.push_back(std::make_pair(&graph.at(row - 1).at(col),
			(random) ? ((randomDistance(eng)) ? 1 : 2) : 1));
	}
	if (col < yMax - 1)
	{
		node.m_Neighbors.push_back(std::make_pair(&graph.at(row).at(col + 1),
			(random) ? ((randomDistance(eng)) ? 1 : 2) : 1));
	}
	if (row < xMax - 1)
	{
		node.m_Neighbors.push_back(std::make_pair(&graph.at(row + 1).at(col),
			(random) ? ((randomDistance(eng)) ? 1 : 2) : 1));
	}
}

void AStar::ComputeGraphHeuristics()
{
	for (auto &row : graph)
	{
		for (auto &node : row)
		{
			ComputeNodeHeuristic(&node);
		}
	}
}

void AStar::ComputeNodeHeuristic(Node *node)
{
	node->m_Heuristic = std::abs(node->m_Position.x - endNode.x) + std::abs(node->m_Position.y - endNode.y);
}

void AStar::Clean()
{
	end = false;

	/*
	for (auto &row : graph)
	{
		for (auto &node : row)
		{
			node.m_Neighbors.clear();
			node.m_Parent = nullptr;
			node.m_WeightSum = 0;
			node.m_NodeState = Unknown;
			node.m_Heuristic = 0;
			node.m_Position.x = 0;
			node.m_Position.y = 0;
		}
	}
	Volendo fare random ogni volta la clean deve anche rifare tutti i nodi
	*/
	graph.clear();

	openList = std::list<Node*>();
}

void AStar::DrawStep(sf::RenderWindow & window)
{
	Node *finalNodeVero = &graph.at(endNode.x).at(endNode.y);
	const Node *beginNode = &graph.at(startNode.x).at(startNode.y);

	for (int i = 0; i < xMax; ++i)
	{
		for (int j = 0; j < yMax; ++j)
		{
			sf::RectangleShape square;
			square.setSize({ 30.0f, 30.0f });
			square.setPosition({ j*30.0f + 350.0f,i*30.0f + 50.0f });
			Vector2 here(i, j);
			Node *notInPath = &graph.at(i).at(j);
			if (notInPath->m_NodeState == Closed)
			{
				square.setFillColor(sf::Color::Yellow);
			}
			else if (notInPath->m_NodeState == Open)
			{
				square.setFillColor(sf::Color::Green);
			}
			else
			{
				square.setFillColor(sf::Color::Blue);
			}
			window.draw(square);
		}
	}
	sf::RectangleShape square;
	square.setSize({ 30.0f, 30.0f });
	square.setPosition({ finalNodeVero->m_Position.y*30.0f + 350.0f,finalNodeVero->m_Position.x*30.0f + 50.0f });
	square.setFillColor(sf::Color::Cyan);
	window.draw(square);
	square.setPosition({ beginNode->m_Position.y*30.0f + 350.0f,beginNode->m_Position.x*30.0f + 50.0f });
	square.setFillColor(sf::Color::Magenta);
	window.draw(square);
	
}

void AStar::Search()
{
	Node *finalNode = &graph.at(endNode.x).at(endNode.y);
	bool finalNodeFound = false;

	while (!openList.empty() && !finalNodeFound)
	{
		Node *toCheck = *openList.begin();
		openList.erase(openList.begin());
		if (toCheck == finalNode)
		{
			finalNodeFound = true;
		}
		else
		{
			VisitNode(toCheck);
		}
	}

	if (!finalNodeFound)
	{
		std::cout << "Not found" << std::endl;
	}
	else
	{
		PrintPath(finalNode);
	}
	end = true;

}

void AStar::VisitNode(Node *toVisit)
{
	toVisit->m_NodeState = Closed;
	for (Node::DistanceNode adj : toVisit->m_Neighbors)
	{
		AddNodeToOpenList(toVisit, adj.first, adj.second);
	}
}

void AStar::AddNodeToOpenList(Node *parent, Node *node, int weightToGo)
{
	if (node->m_NodeState == Unknown || node->m_WeightSum > parent->m_WeightSum + weightToGo)
	{
		node->m_WeightSum = parent->m_WeightSum + weightToGo;
		node->m_Parent = parent;
		node->m_NodeState = Open;

		auto toCompare = openList.begin();
		auto end = openList.end();
		while ((toCompare != end) && (node->m_WeightSum + node->m_Heuristic > (*toCompare)->m_WeightSum + (*toCompare)->m_Heuristic))
		{
			++toCompare;
		}
		openList.insert(toCompare, node);

	}
}

void AStar::Step()
{
	Node *finalNode = &graph.at(endNode.x).at(endNode.y);
	bool finalNodeFound = false;

	if (!openList.empty())
	{
		Node *toCheck = *openList.begin();
		openList.erase(openList.begin());
		if (toCheck == finalNode)
		{
			finalNodeFound = true;
		}
		else
		{
			VisitNode(toCheck);
		}
	}

	if (finalNodeFound){
		end = true;
	}
}

void AStar::PrintPath(Node *node) const
{
	std::vector<Vector2> positions;
	const Node *beginNode = &graph.at(startNode.x).at(startNode.y);

	while (node != beginNode)
	{
		positions.push_back(node->m_Position);
		node = node->m_Parent;
	}
	positions.push_back(beginNode->m_Position);

	for (int i = 0; i < xMax; ++i)
	{
		for (int j = 0; j < yMax; ++j)
		{
			Vector2 here(i, j);
			if (std::find(positions.begin(), positions.end(), here) != positions.end())
			{
				std::cout << "X";
			}
			else
			{
				std::cout << "-";
			}
			std::cout << " ";
		}
		std::cout << std::endl;
	}
}

void AStar::DrawPath(sf::RenderWindow &window)
{
	Node *finalNodeVero = &graph.at(endNode.x).at(endNode.y);
	Node *finalNode = &graph.at(endNode.x).at(endNode.y);

	if (finalNode->m_Parent != nullptr)
	{
		std::vector<Vector2> positions;
		const Node *beginNode = &graph.at(startNode.x).at(startNode.y);

		while (finalNode != beginNode)
		{
			positions.push_back(finalNode->m_Position);
			finalNode = finalNode->m_Parent;
		}
		positions.push_back(beginNode->m_Position);

		for (int i = 0; i < xMax; ++i)
		{
			for (int j = 0; j < yMax; ++j)
			{
				sf::RectangleShape square;
				square.setSize({ 30.0f, 30.0f });
				square.setPosition({ j*30.0f + 350.0f,i*30.0f + 50.0f });
				Vector2 here(i, j);
				if (std::find(positions.begin(), positions.end(), here) != positions.end())
				{
					square.setFillColor(sf::Color::Red);
				}
				else
				{
					Node *notInPath = &graph.at(i).at(j);
					if (notInPath->m_NodeState == Closed)
					{
						square.setFillColor(sf::Color::Black);
					}
					else if (notInPath->m_NodeState == Open)
					{
						square.setFillColor(sf::Color::Green);
					}
					else
					{
						square.setFillColor(sf::Color::Blue);
					}
				}
				window.draw(square);
			}
		}
		sf::RectangleShape square;
		square.setSize({ 30.0f, 30.0f });
		square.setPosition({ finalNodeVero->m_Position.y*30.0f + 350.0f,finalNodeVero->m_Position.x*30.0f + 50.0f });
		square.setFillColor(sf::Color::Cyan);
		window.draw(square);
		square.setPosition({ beginNode->m_Position.y*30.0f + 350.0f,beginNode->m_Position.x*30.0f + 50.0f });
		square.setFillColor(sf::Color::Magenta);
		window.draw(square);
		
	}

}
