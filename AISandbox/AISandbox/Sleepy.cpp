#include "Sleepy.h"


const std::string Sleepy::NAME = "Sleepy";

//public

Sleepy::Sleepy() : Sleepy(0, MAX_ENERGY)
{
}

Sleepy::Sleepy(const int i_InitialLoad, const int i_InitialEnergy) : Miner(NAME, MAX_LOAD, MAX_ENERGY, i_InitialLoad, i_InitialEnergy)
{
}

void Sleepy::Extract()
{
	Miner::Extract(1, 1);
}

void Sleepy::Deposit()
{
	Miner::Deposit(2);
}

void Sleepy::Recover()
{
	Miner::Recover(2);
}
