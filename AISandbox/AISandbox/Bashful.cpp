#include "Bashful.h"


const std::string Bashful::NAME = "Bashful";

//public

Bashful::Bashful() : Bashful(0, MAX_ENERGY)
{
}

Bashful::Bashful(const int i_InitialLoad, const int i_InitialEnergy) : Miner(NAME, MAX_LOAD, MAX_ENERGY, i_InitialLoad, i_InitialEnergy)
{
}

void Bashful::Extract()
{
	Miner::Extract(1, 1);
}

void Bashful::Deposit()
{
	Miner::Deposit(1);
}

void Bashful::Recover()
{
	Miner::Recover(1);
}
