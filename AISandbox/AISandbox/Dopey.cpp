#include "Dopey.h"


const std::string Dopey::NAME = "Dopey";

//public

Dopey::Dopey() : Dopey(0, MAX_ENERGY)
{
}

Dopey::Dopey(const int i_InitialLoad, const int i_InitialEnergy) : Miner(NAME, MAX_LOAD, MAX_ENERGY, i_InitialLoad, i_InitialEnergy)
{
}

void Dopey::Extract()
{
	Miner::Extract(1, 5);
}

void Dopey::Deposit()
{
	Miner::Deposit(10);
}

void Dopey::Recover()
{
	Miner::Recover(2);
}
