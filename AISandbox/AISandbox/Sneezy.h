#pragma once

#include "Miner.h"

class Sneezy : public Miner
{

private:

	static const int MAX_LOAD = 30;
	static const int MAX_ENERGY = 10;

	static const std::string NAME;

public:

	Sneezy();
	Sneezy(const int i_InitialLoad, const int i_InitialEnergy);

	virtual void Extract() override;
	virtual void Deposit() override;
	virtual void Recover() override;

};