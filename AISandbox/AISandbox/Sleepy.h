#pragma once

#include "Miner.h"

class Sleepy : public Miner
{

private:

	static const int MAX_LOAD = 15;
	static const int MAX_ENERGY = 15;

	static const std::string NAME;

public:

	Sleepy();
	Sleepy(const int i_InitialLoad, const int i_InitialEnergy);

	virtual void Extract() override;
	virtual void Deposit() override;
	virtual void Recover() override;

};