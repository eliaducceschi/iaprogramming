#pragma once

#include "Node.h"
#include <SFML/Graphics.hpp>

#include <iostream>
#include <list>
#include <vector>
#include <random>

class AStar
{

public:

	bool end = false;

	void Run(int xMax, int yMax, Vector2 startNode, Vector2 endNode, bool random);

	void Clean();
	void DrawStep(sf::RenderWindow& window);
	void DrawPath(sf::RenderWindow& window);

	void Step();

private:

	static std::default_random_engine eng;

	int xMax = 10;
	int yMax = 10;
	Vector2 startNode;
	Vector2 endNode;
	bool random = false;

	void CreateGraph();
	void CreateGraphAdjs();
	void CreateNodeAdj(const int iRow, const int iCol);

	void ComputeGraphHeuristics();
	void ComputeNodeHeuristic(Node* pNode);

	void Search();
	void VisitNode(Node *toVisit);
	void AddNodeToOpenList(Node* pParent, Node* pNode, int weightToGo);

	void PrintPath(Node* pNode) const;

	std::vector<std::vector<Node>> graph;

	std::list<Node*> openList;
};
