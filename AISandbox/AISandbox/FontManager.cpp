#include "FontManager.h"
#include <cassert>

void FontManager::Init()
{
	sf::Font sansation;
	sansation.loadFromFile("AISandbox/Sansation.ttf");
	m_fonts.insert(std::pair<FontEnum, sf::Font>(FontEnum::Font_Sansation, sansation));

	sf::Font verdana;
	verdana.loadFromFile("AISandbox/Verdana.ttf");
	m_fonts.insert(std::pair<FontEnum, sf::Font>(FontEnum::Font_Verdana, verdana));
}

void FontManager::Destroy()
{

}

const sf::Font* FontManager::GetFont(const FontEnum eFont) const
{
	FontMap::const_iterator font = m_fonts.find(eFont);
	assert( font != m_fonts.end() && "Fonts: GetFont < Font not found >" );

	return &font->second;
}
