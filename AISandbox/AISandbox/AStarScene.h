#pragma once

#include "IScene.h"
#include "FontManager.h"
#include <assert.h>
#include <vector>
#include <list>
#include <time.h>
#include <sstream>
#include "AStar.h"



class AStarScene : public IScene
{
private:

	AStar alg;

public:
    
	AStarScene();

	virtual void OnEnter();
	virtual void OnIdle(void);
	virtual void OnDraw(sf::RenderWindow&);
    
	virtual ~AStarScene(void);

};


