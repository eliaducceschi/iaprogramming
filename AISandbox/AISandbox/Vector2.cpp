#include "Vector2.h"

const Vector2 Vector2::ZERO(0, 0);
const Vector2 Vector2::X_VERSOR(1, 0);
const Vector2 Vector2::Y_VERSOR(0, 1);
