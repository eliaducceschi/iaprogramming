#include "DepositState.h"
#include "HomeState.h"

DepositState * DepositState::m_Instance = nullptr;

//public

DepositState* DepositState::GetInstance()
{
	if (m_Instance == nullptr)
	{
		m_Instance = new DepositState();
	}
	return m_Instance;
}

void DepositState::Enter(Miner *i_Agent)
{
}

State<Miner>* DepositState::Tick(Miner *i_Agent)
{
	if (i_Agent->HasNoLoad())
	{
		return HomeState::GetInstance();
	}
	i_Agent->Deposit();
	return nullptr;
}

void DepositState::Exit(Miner *i_Agent)
{
}

std::string DepositState::GetDescription() const
{
	return "Deposit";
}

DepositState::~DepositState()
{
}
