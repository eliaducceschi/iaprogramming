#include "Grumpy.h"


const std::string Grumpy::NAME = "Grumpy";

//public

Grumpy::Grumpy() : Grumpy(0, MAX_ENERGY)
{
}

Grumpy::Grumpy(const int i_InitialLoad, const int i_InitialEnergy) : Miner(NAME, MAX_LOAD, MAX_ENERGY, i_InitialLoad, i_InitialEnergy)
{
}

void Grumpy::Extract()
{
	Miner::Extract(10, 10);
}

void Grumpy::Deposit()
{
	Miner::Deposit(2);
}

void Grumpy::Recover()
{
	Miner::Recover(1);
}
