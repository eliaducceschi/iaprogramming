#include "FifteenNode.h"
#include "FontEnum.h"
#include "FontManager.h"

const unsigned int FifteenNode::FONT_SIZE = 20;
const sf::Color FifteenNode::FONT_COLOR = sf::Color(0, 0, 0);
const float FifteenNode::CELL_SIZE = 40.0f;
const float FifteenNode::LINE_THICKNESS = 2.0f;
const sf::Color FifteenNode::LINE_COLOR = sf::Color(0, 0, 0);

//public

FifteenNode::FifteenNode() : m_Parent(nullptr), m_WeightSum(0), m_NullValuePosition(0,0)
{
	for (int i = 0; i < ROWS; ++i)
	{
		for (int j = 0; j < COLUMNS; ++j)
		{
			m_Grid[i][j] = 0;
		}
	}
}

FifteenNode::FifteenNode(const int i_Grid[ROWS][COLUMNS])
{
	Initialize(i_Grid);
}

FifteenNode::FifteenNode(const FifteenNode &i_Other) : m_Parent(i_Other.m_Parent),
m_WeightSum(i_Other.m_WeightSum), m_NullValuePosition(i_Other.m_NullValuePosition)
{
	for (int i = 0; i < ROWS; ++i)
	{
		for (int j = 0; j < COLUMNS; ++j)
		{
			m_Grid[i][j] = i_Other.m_Grid[i][j];
		}
	}
}

void FifteenNode::Initialize(const int i_Grid[ROWS][COLUMNS])
{
	m_Parent = nullptr;
	m_WeightSum = 0;

	for (int i = 0; i < ROWS; ++i)
	{
		for (int j = 0; j < COLUMNS; ++j)
		{
			m_Grid[i][j] = i_Grid[i][j];
			if (i_Grid[i][j] == NULL_VALUE)
			{
				m_NullValuePosition = { i, j };
			}
		}
	}
}

bool FifteenNode::operator==(const FifteenNode & i_Other) const
{
	bool equals = true;

	for (int i = 0; i < ROWS && equals; ++i)
	{
		for (int j = 0; j < COLUMNS && equals; ++j)
		{
			if (m_Grid[i][j] != i_Other.m_Grid[i][j])
			{
				equals = false;
			}
		}
	}

	return equals;
}

bool FifteenNode::operator!=(const FifteenNode & i_Other) const
{
	return !(*this == i_Other);
}

bool FifteenNode::operator<(const FifteenNode & i_Other) const
{
	return m_WeightSum < i_Other.m_WeightSum;
}

std::vector<FifteenNode> FifteenNode::GetNeighbors() const
{
	std::vector<FifteenNode> neighbors;

	const int x = m_NullValuePosition.x;
	const int y = m_NullValuePosition.y;

	if (x > 0)
	{
		neighbors.push_back(GetNeighbor({ x - 1, y }));
	}
	if (x < ROWS - 1)
	{
		neighbors.push_back(GetNeighbor({ x + 1, y }));
	}
	if (y > 0)
	{
		neighbors.push_back(GetNeighbor({ x, y - 1 }));
	}
	if (y < COLUMNS - 1)
	{
		neighbors.push_back(GetNeighbor({ x, y + 1 }));
	}

	return neighbors;
}

int FifteenNode::GetGPlusH(const FifteenNode & i_EndNode) const
{
	return m_WeightSum + CalculateHeuristic(i_EndNode);
}

void FifteenNode::Draw(sf::RenderWindow & i_RenderWindow, float i_XOffset, float i_YOffset) const
{
	DrawGrid(i_RenderWindow, i_XOffset, i_YOffset);

	const sf::Font* font = FontManager::Istance()->GetFont(FontEnum::Font_Sansation);

	for (int i = 0; i < ROWS; ++i)
	{
		for (int j = 0; j < COLUMNS; ++j)
		{
			if (m_Grid[i][j] != NULL_VALUE)
			{
				sf::Text number = sf::Text(std::to_string(m_Grid[i][j]), (*font), FONT_SIZE);
				number.setOrigin(number.getLocalBounds().width / 2.0f, number.getLocalBounds().height / 2.0f);
				number.setFillColor(FONT_COLOR);

				float x = i_XOffset + (CELL_SIZE + LINE_THICKNESS) * j + CELL_SIZE / 2.0f;
				float y = i_YOffset + (CELL_SIZE + LINE_THICKNESS) * i + CELL_SIZE / 2.0f;
				number.setPosition({ x,y });

				i_RenderWindow.draw(number);
			}
		}
	}
}

//private

int FifteenNode::CalculateHeuristic(const FifteenNode & i_EndNode) const
{
	int heuristic = 0;

	for (int i = 0; i < ROWS; ++i)
	{
		for (int j = 0; j < COLUMNS; ++j)
		{
			heuristic += CalculateDistance({i, j}, i_EndNode);
		}
	}

	return 0;
}

int FifteenNode::CalculateDistance(const Vector2 i_GridPos, const FifteenNode & i_Node) const
{
	const int &x = i_GridPos.x;
	const int &y = i_GridPos.y;
	const int &value = m_Grid[x][y];

	int distance = 0;
	bool found = false;

	for (int i = 0; i < ROWS && !found; ++i)
	{
		for (int j = 0; j < COLUMNS && !found; ++j)
		{
			if (i_Node.m_Grid[i][j] == value)
			{
				distance = std::abs(x - i) + std::abs(y - j);
				found = true;
			}
		}
	}

	return distance;
}

FifteenNode FifteenNode::GetNeighbor(Vector2 i_ToSwap) const
{
	FifteenNode neighbor(m_Grid);

	std::swap(neighbor.m_Grid[m_NullValuePosition.x][m_NullValuePosition.y], neighbor.m_Grid[i_ToSwap.x][i_ToSwap.y]);
	neighbor.m_NullValuePosition = i_ToSwap;
	neighbor.m_Parent = this;
	neighbor.m_WeightSum = m_WeightSum + 1;

	return neighbor;
}

void FifteenNode::DrawGrid(sf::RenderWindow &i_RenderWindow, float i_XOffset, float i_YOffset) const
{
	sf::RectangleShape horizontal;
	horizontal.setSize({ COLUMNS * (CELL_SIZE + LINE_THICKNESS), LINE_THICKNESS });
	horizontal.setFillColor(LINE_COLOR);

	for (int i = 1; i < ROWS; ++i)
	{
		float x = i_XOffset;
		float y = i_YOffset - LINE_THICKNESS + (CELL_SIZE + LINE_THICKNESS) * i;
		horizontal.setPosition(x, y);
		i_RenderWindow.draw(horizontal);
	}

	sf::RectangleShape vertical;
	vertical.setSize({ LINE_THICKNESS, ROWS * (CELL_SIZE + LINE_THICKNESS) });
	vertical.setFillColor(LINE_COLOR);
	for (int i = 1; i < ROWS; ++i)
	{
		float x = i_XOffset - LINE_THICKNESS + (CELL_SIZE + LINE_THICKNESS) * i;
		float y = i_YOffset;
		vertical.setPosition(x, y);
		i_RenderWindow.draw(vertical);
	}
}

