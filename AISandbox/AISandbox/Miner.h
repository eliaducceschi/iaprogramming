#pragma once

#include "FiniteStateMachine.h"

#include <SFML/Graphics.hpp>
#include <string>

class Miner
{

private:

	FiniteStateMachine<Miner> m_Brain;

	std::string m_Name;

	int m_MaxLoad;
	int m_MaxEnergy;

	int m_Deposit;
	int m_Load;
	int m_Energy;

public:

	Miner(std::string i_Name, int i_MaxLoad, int i_MaxEnergy);
	Miner(std::string i_Name, int i_MaxLoad, int i_MaxEnergy, int i_InitialLoad, int i_InitialEnergy);

	void Tick();

	virtual void Extract() = 0;
	virtual void Deposit() = 0;
	virtual void Recover() = 0;

	void Draw(sf::RenderWindow &i_RenderWindow, float i_StartX, float i_StartY);

	void Reset();
	void Reset(int i_Load, int i_Energy);

	bool Miner::IsFullLoad() const;
	bool Miner::IsFullEnergy() const;

	bool Miner::IsTired() const;
	bool Miner::HasNoLoad() const;

	std::string GetName() const;
	std::string GetState() const;
	int GetMaxLoad() const;
	int GetMaxEnergy() const;
	int GetLoad() const;
	int GetEnergy() const;
	int GetDeposit() const;

protected:

	void Extract(int i_ToExtract, int i_EnergyUsed);
	void Deposit(int i_Quantity);
	void Recover(int i_Quantity);

};