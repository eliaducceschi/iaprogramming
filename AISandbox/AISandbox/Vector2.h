#pragma once
#include <math.h>

/*
	Bidimensional Vector and some util methods.
*/

class Vector2
{
public:
	static const Vector2 ZERO;
	static const Vector2 X_VERSOR;
	static const Vector2 Y_VERSOR;

	Vector2() 
	{
		x = 0; y = 0;
	}

	Vector2(int x, int y)
	{
		this->x = x;
		this->y = y;
	}

	Vector2(const Vector2& other)
	{
		this->x = other.x;
		this->y = other.y;
	}

	int		x;
	int		y;

	inline Vector2&	operator+=(const Vector2& other)
	{
		x += other.x;
		y += other.y;
		
		return *this;
	}

	inline Vector2 operator-=(const Vector2& other)
	{
		x -= other.x;
		y -= other.y;

		return *this;
	}

	inline Vector2 operator*=(const Vector2& other)
	{
		x *= other.x;
		y *= other.y;

		return *this;
	}

	inline Vector2 operator*=(const int& rhs)
	{
		x *= rhs;
		y *= rhs;

		return *this;
	}

	inline Vector2 operator/=(const Vector2& other)
	{
		x /= other.x;
		y /= other.y;

		return *this;
	}

	bool operator==(const Vector2& other)const
	{
		return ( x == other.x && y == other.y );
	}

	bool operator!=(const Vector2& other)const
	{
		return (x != other.x) || (y != other.y);
	}

	bool isZero()const{ return (x==0 && y==0);}

	void Normalize()
	{
		int length = Length();
		if(length>0){
			x /= length;
			y /= length;
		}
	}
	Vector2 NormalizeCopy()
	{
		Vector2 temp(x, y);
		temp.Normalize();
		return temp;
	}

	int		DotProduct(const Vector2& other) const
	{
		return x * other.x + y * other.y;
	}
	
	int		Length() const
	{
		return static_cast<int>(SquaredLength());
	}

	int		SquaredLength() const
	{
		return x * x + y * y;
	}

	void		Set(int x, int y) { this->x = x; this->y = y; }
	void		Zero(){ Set(0, 0); }

	inline void Truncate(int max)
	{
		if (this->Length() > max)
		{
			this->Normalize();

			*this *= max;
		} 
	}

	inline int Distance(const Vector2& v2)const
	{
		int ySeparation = v2.y - y;
		int xSeparation = v2.x - x;

		return static_cast<int>(sqrt(ySeparation*ySeparation + xSeparation*xSeparation));
	}

};

inline Vector2 operator*(const Vector2 &lhs, int rhs);
inline Vector2 operator*(int lhs, const Vector2 &rhs);
inline Vector2 operator-(const Vector2 &lhs, const Vector2 &rhs);
inline Vector2 operator+(const Vector2 &lhs, const Vector2 &rhs);
inline Vector2 operator/(const Vector2 &lhs, int val);

inline Vector2 operator*(const Vector2 &lhs, int rhs)
{
	Vector2 result(lhs);
	result *= rhs;
	return result;
}

inline Vector2 operator*(int lhs, const Vector2 &rhs)
{
	Vector2 result(rhs);
	result *= lhs;
	return result;
}

inline Vector2 operator-(const Vector2 &lhs, const Vector2 &rhs)
{
	Vector2 result(lhs);
	result.x -= rhs.x;
	result.y -= rhs.y;

	return result;
}

inline Vector2 operator+(const Vector2 &lhs, const Vector2 &rhs)
{
	Vector2 result(lhs);
	result.x += rhs.x;
	result.y += rhs.y;

	return result;
}

inline Vector2 operator/(const Vector2 &lhs, int val)
{
	Vector2 result(lhs);
	result.x /= val;
	result.y /= val;

	return result;
}